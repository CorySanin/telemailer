FROM node:14-alpine

WORKDIR /usr/src/telemailer

COPY . .

RUN npm install

CMD [ "node", "index.js"]
