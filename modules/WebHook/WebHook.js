const path = require('path');
const crypto = require('crypto');
const EventEmitter = require('events');
const express = require('express');
const phin = require('phin');
const vsprintf = require('sprintf-js').vsprintf;

const DEFAULT_PORT = 8080;
const domainregex = new RegExp(/https?:\/\/([^/]+)/);

function traverseObject(obj, arr) {
    let ret = obj;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] in ret) {
            ret = ret[arr[i]];
        }
        else {
            return null;
        }
    }
    return ret;
}

function createMessage(str, replacearr, obj) {
    let arr = [];
    for (let i = 0; i < replacearr.length; i++) {
        arr.push(traverseObject(obj, replacearr[i]));
    }
    return vsprintf(str, arr);
}

class WebHook extends EventEmitter {
    _server;

    /**
     * Web server for creating messages from webhook payloads
     * @param {*} options 
     *      number port: port to run server on
     *      {*}[] configs:
     *          string endpoint: endpoint path for this configuration
     * 
     *          string message: message to emit on success (uses sprintf-js https://www.npmjs.com/package/sprintf-js#format-specification )
     *          string[][] replacements: array json traversal paths to replace in the message
     * 
     *          'none'|'ip'|'token'|'digest'|'callback' type: method used for validating a webhook post
     * 
     *          string[] ips: IPs to accept payloads from (type must be 'ip')
     * 
     *          if type is 'token':
     *          string token: token to check for
     *          string tokenheader: header to check the token against
     * 
     *          if type is 'digest':
     *          string token: token to use
     *          string hash: algorithm to use for generating digest
     *          string digesttype: how to represent the digest
     *          string signatureheader: name of header used for signature
     *          
     *          if type is 'callback' (note: this method sucks):
     *          string callbackdomain: the domain to expect from the callback url
     *          string[] callbackpath: the path to traverse to get the callback url
     *          string[] callbackstatuspath: the path to traverse to get the callback state
     *          any callbacksuccess: the value to determine if the callback was successful
     */
    constructor(options = {}) {
        super();

        this._server = express();
        this._server.use(express.json());

        if ('configs' in options) {
            options.configs.forEach(config => {
                let func;

                if (config.type === 'callback') {
                    func = async (req, res) => {
                        let callback = traverseObject(req.body, config.callbackpath);
                        let domainregmatch = domainregex.exec(callback);
                        if (domainregmatch[1].toLowerCase() === config.callbackdomain.toLowerCase()) {
                            let callbackres = await phin({
                                url: callback,
                                method: 'POST',
                                data: {
                                    callback: true
                                },
                                parse: 'json'
                            });
                            let callbacksuccess = traverseObject(callbackres, config.callbackstatuspath);
                            if (callbacksuccess === config.callbacksuccess) {
                                req.body['_callback'] = callbackres;
                                this.emit('message', createMessage(config.message, config.replacements, req.body));
                            }
                        }
                        res.send('OK');
                    }
                }
                else {
                    func = (req, res) => {
                        if ((config.type === 'ip' && config.ips.indexOf(req.ip) >= 0) ||
                            (config.type === 'token' && crypto.timingSafeEqual(req.get(config.tokenheader), config.token)) ||
                            (config.type === 'digest' && crypto.timingSafeEqual(req.get(config.signatureheader), crypto.createHmac(('hash' in config) ? config.hash : 'sha1', config.token).update(req.body).digest(('digesttype' in config) ? config.digesttype : 'hex'))) ||
                            (config.type === 'none')) {
                            this.emit('message', createMessage(config.message, config.replacements, req.body));
                        }
                        res.send('OK');
                    }
                }

                this._server.post(config.endpoint, func);
            });
        }

        this._server.listen(('port' in options) ? options.port : DEFAULT_PORT, () => {
            this.emit('ready');
        });
    }

    close() {
        this._server.close();
    }
}

module.exports = exports = WebHook;