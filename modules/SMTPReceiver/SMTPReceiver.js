const EventEmitter = require('events');
const SMTPServer = require('smtp-server').SMTPServer;
const simpleParser = require('mailparser').simpleParser;

const DEFAULT_PORT = 25;

async function processMessage(stream, session, cb) {
    const chunks = [];
    return new Promise((resolve, reject) => {
        stream.on('data', chunk => chunks.push(Buffer.from(chunk)));
        stream.on('error', reject);
        stream.on('end', async () => {
            let parsed = await simpleParser(Buffer.concat(chunks).toString('utf8'));
            cb();
            resolve(`<b><i>${session.user}:</i></b> ${parsed.subject}<br/>${parsed.html}`);
        });
    });
}

class SMTPReceiver extends EventEmitter {
    _server;

    constructor(options = {}) {
        super();
        let smtpoptions = {
            secure: false,
            authMethods: ['PLAIN'],
            allowInsecureAuth: false,
            hideSTARTTLS: true,
            onMailFrom: (address, session, callback) => {
                callback();
            },
            onAuth: (auth, session, callback) => {
                callback(null, {
                    user: auth.username
                });
            },
            onData: async (stream, session, callback) => {
                let message = await processMessage(stream, session, callback);
                this.emit('message', message);
            }
        }

        this._server = new SMTPServer(smtpoptions);

        this._server.listen('port' in options ? options.port : DEFAULT_PORT);
    }

    close() {
        this._server.close();
    }
}

module.exports = exports = SMTPReceiver;