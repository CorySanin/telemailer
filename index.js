const fs = require('fs/promises');
const path = require('path');
const LOG = require("log");
const json5 = require('json5');
const TelegramBot = require('./TelegramBot');
const DEFAULT_CONFIG_PATH = path.join(__dirname, 'config', 'config.json5');
const Modules = {};
const moduleInstances = [];
require("log-node")();
let log = LOG.get("index");

async function main() {
    const config = json5.parse(await fs.readFile(DEFAULT_CONFIG_PATH));
    const bot = new TelegramBot(config.bot);
    config.modules.forEach(module => {
        if(!(module.module in Modules)){
            Modules[module.module] = require(path.join(__dirname, 'modules', module.module));
        }
        let m = new Modules[module.module](module.config);
        m.on('message', bot.sendMessage);
        moduleInstances.push(m);
    });
    log.info('Telemailer has been initialized');
}

main();