const path = require('path');
const fs = require('fs/promises');
const LOG = require("log");
const json5 = require('json5');
const TG = require('telegram-bot-api');
const linebreak = /<br\s*\/?>/g
const htmltags = /<\/?\w[^>]*>/g;
const goodhtmltags = /^<\/?(b|strong|i|em|u|ins|s|strike|del|a|code|pre)(\s[^>]*)?>$/;

let log = LOG.get("telegrambot");

// https://core.telegram.org/bots/api#html-style
function stripInvalidHtml(html) {
    let s = html.replace(linebreak, '<pre>\n</pre>');
    let matches = s.match(htmltags);
    for (let i = 0; i < matches.length; i++) {
        if (!matches[i].match(goodhtmltags)) {
            s = s.replace(matches[i], '');
        }
    }
    return s;
}

class TelegramBot {
    _chats = [];
    _password = '';
    _maxclients = 0;
    _initialized = false;
    _chatsfileLocation = path.join(__dirname, 'config', 'chats.json5');

    constructor(options = {}) {
        if ('token' in options) {
            let mp = new TG.GetUpdateMessageProvider();

            if ('password' in options) {
                this._password = options.password;
            }
            if ('maxclients' in options) {
                this._maxclients = options.maxclients;
            }
            if ('chatsfile' in options) {
                this._chatsfileLocation = options.chatsfile;
            }

            this._bot = new TG({
                token: options.token
            });
            this._bot.setMessageProvider(mp);
            this._bot.on('update', update => {
                log.info('Message received from client');
                let message = ('message' in update && 'text' in update.message) ? update.message.text : '';
                if (this._password !== '' && (message === '/subscribe' || message === '/start')) {
                    this._bot.sendMessage({
                        chat_id: update.message.chat.id,
                        text: 'Please send the password to subscribe'
                    })
                }
                else if (message === '/unsubscribe') {
                    this.unsubscribe(update.message.chat.id);
                    this._bot.sendMessage({
                        chat_id: update.message.chat.id,
                        text: 'You have been unsubscribed'
                    });
                }
                else if (this._password === '' || message === this._password) {
                    if (this.subscribe(update.message.chat.id)) {
                        this._bot.sendMessage({
                            chat_id: update.message.chat.id,
                            text: 'You have subscribed'
                        });
                    }
                    else if (this._chats.indexOf(update.message.chat.id) === -1) {
                        this._bot.sendMessage({
                            chat_id: update.message.chat.id,
                            text: 'Subscription failed.'
                        });
                    }
                }
                else if (this._chats.indexOf(update.message.chat.id) === -1) {
                    this._bot.sendMessage({
                        chat_id: update.message.chat.id,
                        text: 'Subscription failed.'
                    });
                }
            });
            this.loadChatsFile();
            this._bot.start();
            this.initializeBot();
        }
    }

    initializeBot = async () => {
        if (!this._initialized) {
            await this._bot.setMyCommands({
                commands: [
                    { command: 'subscribe', description: 'Subscribe to notifications' },
                    { command: 'unsubscribe', description: 'Unsubscribe to notifications' }
                ]
            });
            this._initialized = true;
        }
    }

    sendMessage = (text, mode = 'HTML') => {
        this._chats.forEach(chat => {
            this._bot.sendMessage({
                chat_id: chat,
                parse_mode: mode,
                text: stripInvalidHtml(text)
            }).then((result) => {
                log.info('Message sent successfully');
            }).catch((err) => {
                log.error(err);
            });
        });
    }

    subscribe = (chatid) => {
        if ((this._maxclients === 0 || this._maxclients > this._chats.length) && this._chats.indexOf(chatid) === -1) {
            this._chats.push(chatid);
            this.writeChatsFile();
            return true;
        }
        else {
            return false;
        }
    }

    unsubscribe = (chatid) => {
        let index = this._chats.indexOf(chatid)
        if (index > -1) {
            this._chats.splice(index, 1);
            this.writeChatsFile();
            return true;
        }
    }

    loadChatsFile = async () => {
        try {
            const config = json5.parse(await fs.readFile(this._chatsfileLocation));
            if ('chats' in config) {
                this._chats = config.chats;
            }
        }
        catch {
            log.warning(`chats file ${this._chatsfileLocation} could not be read`)
            await this.writeChatsFile();
        }
    }

    writeChatsFile = async () => {
        await fs.writeFile(this._chatsfileLocation, JSON.stringify({
            chats: this._chats
        }));
    }
}

module.exports = exports = TelegramBot;